# Front-end

Vue.js-based front-end of Fretadão's challenge

Tools:
* **Node v10.16.3**
* **Vue 3.11.0**

## Setting up the environment
Docker/Docker Compose sets the environment configuration. So, the prerequisites are:

-  [Docker](https://docs.docker.com/install/)
-  [Docker Compose](https://docs.docker.com/compose/install/)

Run it with:  

```bash

docker-compose up

```

This command downloads the Node image first if you haven't them already. It installs the npm dependencies too.

After the download, you can navigate to `http://localhost:8080.`

## Usage
### Searching
On the Homepage, you can search for users. The search engine matches match name, Twitter username, and Twitter description. An empty search returns **all objects**.

On the `SearchResults` view, you'll see the users matched. The thumbnail pictures are fetched from https://randomuser.me. You can see or edit a profile.

### Show/Edit
Here you can choose to show or edit a profile. The only editable fields are `name` and `Twitter address` since the others are fetched/created by the API. On the edit mode, you can edit the field, and **delete** the profile. You'll be asked for confirmation to do this action.

### Adding a new profile
To add a new profile, go to `Add Profile` at the right side of the navbar.
You'll be redirected for the `AddProfile` view, where you can insert the name and Twitter address. This information is validated client-side as well on the server-side. So, you must insert a valid Twitter URL, and both fields can't be empty.

After creating, you'll be redirected for the `Profile` view.


## About the Architecture
I've used Vue.js to create the interface because it is small, fast, and a robust framework. The component-based architecture is an excellent choice to improve code reuse and, thus, avoid bugs. I've used the Vuetify lib to create a `Material Design` interface. The application is a SPA, so changing pages feels smoother, and all the components are mobile-friendly.

### Views/Components
There are four views and one component on the application:

* **Views** (src/views)
    * **AddProfile**
    * **Home**
    * **Profile**
    * **SearchResults**

You can read about these components in the [Usage](#usage) section.

* **Components** (src/components)
    * **ErrorList:** Receives the errors from the API as props and renders them as a list.

### Design Patterns and Good Practices
I've used the Factory design pattern to create endpoints for the API (src/api). The files are explained below:

* **API.js:** This creates a new `axios` instance. It is responsible for making the HTTP requests.
* **APIFactory.js:** The API factory is where the design pattern is applied. Here, based on the user parameter, the Factory returns the desired FactoryInstance. This is a simple project, but on a bigger one, this would substantially improve development, debug, and test.

* **profilesAPI.js:** This file defines the profile endpoints linked with the back-end. This configuration removes this logic from the components. So if one endpoint changes (either the path or the params), the developer would change only this file, not all components that use this endpoint.


#### Components/Mixins
Components are a nice way to make reusable code. They keep the parent component clean and centers the modifications and responsibilities at one place.

Mixins, as well, are a good way to achieve this. I've used some mixins (src/mixins) to add _DRYness_ on my components.


#### Style Guide
I added `eslint` a linter. It's good to keep the code clean, concise, and following a standard.

#### Continuous Integration
I've used `Gitlab CI` to make continuous integration. Each commit triggers the CI that runs the linter and builds the app. A continuous integration could easily be set here too.

#### Version Control
All the work was made using GIT as the version control manager.  
