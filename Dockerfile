FROM node:10-slim

RUN apt-get update -qq && \
    apt-get install -y build-essential

WORKDIR /fretadao_front
