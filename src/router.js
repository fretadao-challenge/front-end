import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import SearchResults from './views/SearchResults.vue';
import AddProfile from './views/AddProfile.vue';
import Profile from './views/Profile.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/search',
      name: 'search-results',
      component: SearchResults,
    },
    {
      path: '/add-profile',
      name: 'add-profile',
      component: AddProfile,
    },
    {
      path: '/profile/:id',
      name: 'profile',
      component: Profile,
    },
  ],
});
