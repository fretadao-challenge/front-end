export const TWITTER_REGEX = /(?:http:\/\/)?(?:www\.)?twitter\.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w-]*\/)*([\w-]*)/i;

const twitterValidation = {
  data: () => ({
    twitterAddressRules: [
      v => !!v || 'Twitter address is required!',
      v => TWITTER_REGEX.test(v) || 'Invalid Twitter address',
    ],
  }),
};

export default twitterValidation;
