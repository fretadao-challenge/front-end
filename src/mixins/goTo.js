const goToMixin = {
  methods: {
    goTo(routeName, params) {
      this.$router.push({ name: routeName, params });
    },
  },
};

export default goToMixin;
