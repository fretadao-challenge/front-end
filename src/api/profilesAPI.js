import API from './API';

const resource = '/profiles';
export default {
  get() {
    return API.get(`${resource}`);
  },
  getProfile(profileId) {
    return API.get(`${resource}/${profileId}`);
  },
  deleteProfile(profileId) {
    return API.delete(`${resource}/${profileId}`);
  },
  createProfile(profileName, twitterAddress) {
    return API.post(`${resource}`, {
      profile: {
        name: profileName,
        twitter_address: twitterAddress,
      },
    });
  },
  editProfile(profileId, profileName, twitterAddress) {
    return API.patch(`${resource}/${profileId}`, {
      profile: {
        name: profileName,
        twitter_address: twitterAddress,
      },
    });
  },
  search(searchTerm) {
    return API.get('/search', {
      params: {
        search_term: searchTerm,
      },
    });
  },
};
