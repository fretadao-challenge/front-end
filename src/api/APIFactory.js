import ProfilesAPI from './profilesAPI';

const apis = {
  profiles: ProfilesAPI,
};

/* eslint-disable import/prefer-default-export */
export const APIFactory = {
  get: name => apis[name],
};
